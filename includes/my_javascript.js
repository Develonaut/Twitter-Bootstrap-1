/*  Ryan McHenry 2/13/2014 Develonaut.com  */

$(document).ready(function() {

    $(".carousel").carousel({interval:6000});
    $(".typeahead").typeahead();


});

function changeSlide(slideNum) {

    $('.carousel').carousel('pause');
    $('.carousel').carousel(slideNum);

}