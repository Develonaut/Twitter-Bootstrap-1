Twitter-Bootstrap-1
===================
The goal here was to familiarize myself with the inner workings and implementation. I’ve spent a few hours on this project, and plan on fleshing it out quite a bit. The best thing about this project is the responsive features bootstrap allows for, and it’s all based off of a 12-grid system, which helps with organization and clean layout. Overall this was a challenge, but one that was incredible and worth every minute of my time.
